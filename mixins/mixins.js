import { mapState } from "vuex";
import { defaultCard } from "@/utils";
export const mixins = {
        $_veeValidate: {
        validator: 'new'
    },
    computed: {
        ...mapState([
            "cards",
            "idcard",
            "creditcard"
        ])
    },
    methods: {
        goto(path) {
            this.$router.push({
                path: path
            });
        },
        OpenModal(idCard, tipo) {
            console.log('Remove')
            this.$store.commit("SET_IDCARD", idCard)
            this.$store.commit("SET_MODAL", { estado: true, tipo: tipo });
        },
        cierraModal() {
            this.$store.commit('SET_MODAL', { estado: false, tipo: 1 })
            this.$store.commit('SET_CREDICARD', defaultCard)
        },
        setAsDefault() {
            let array = this.cards.map((item) => { return item.id == this.idcard ? item.defaultCard = true : item.defaultCard = false })
            this.cierraModal()
        },
        removeCard() {
            let array = this.cards.filter((item) => { return item.id != this.idcard })
            this.$store.commit('SET_CARDS', array)
            this.cierraModal()
        },
        AddCard() {
            this.$validator.validateAll().then(async(result) => {
                if (result) {
                    console.log('Tarjeta añadida', this.creditcard)
                    let array = this.cards
                    array.push(this.creditcard)
                    this.$store.commit('SET_CARDS', array)
                    this.cierraModal()
                    this.$store.commit('SET_CREDICARD', defaultCard)
                }})

            
        },
    }

}