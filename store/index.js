import { defaultModal,creditCards,defaultCard } from "@/utils";
export const strict = false;
export const state = () => ({
    modal:defaultModal,
    idcard:null,
    cards:creditCards,
    creditcard:defaultCard,
    pagina:1
})
export const mutations = {
    SET_MODAL: function(state, payload) {
        state.modal = payload;
    },
    SET_IDCARD:function(state, payload) {
        state.idcard = payload;
    },
    SET_CARDS:function(state,payload){
        state.cards=payload;
    },
    SET_CREDICARD:function(state,payload){
        state.creditcard = {
            ...state.creditcard,
            ...payload
        };
        
    },
    SET_PAGINA:function(state,payload) {
        state.pagina=payload
    }
}
export const actions = {
    async nuxtServerInit({ commit }, { req }) {

    },
    async setModal({ commit }, { payload }) {
        console.log(payload)
        commit("SET_MODAL", payload);
    },
    async setidCard({ commit }, { payload }) {
        console.log(payload)
        commit("SET_IDCARD", payload);
    },
}