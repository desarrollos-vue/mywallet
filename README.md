# MyWallet

> App que permite al usuario agregar tarjetas de credito/debito a una lista, escoger su tarjeta por default y eliminar las que ya no desee tener en la lista

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
