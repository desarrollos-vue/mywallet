import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _7124da62 = () => interopDefault(import('../pages/account/index.vue' /* webpackChunkName: "pages/account/index" */))
const _3e6edbf3 = () => interopDefault(import('../pages/apps/index.vue' /* webpackChunkName: "pages/apps/index" */))
const _7cf33a98 = () => interopDefault(import('../pages/cards/index.vue' /* webpackChunkName: "pages/cards/index" */))
const _30885301 = () => interopDefault(import('../pages/travelers/index.vue' /* webpackChunkName: "pages/travelers/index" */))
const _f342cc78 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/account",
    component: _7124da62,
    name: "account"
  }, {
    path: "/apps",
    component: _3e6edbf3,
    name: "apps"
  }, {
    path: "/cards",
    component: _7cf33a98,
    name: "cards"
  }, {
    path: "/travelers",
    component: _30885301,
    name: "travelers"
  }, {
    path: "/",
    component: _f342cc78,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
